//
// Created by Daria Supriadkina on 04.01.2022.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_MEM_DEBUG_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_MEM_DEBUG_H

#include "stdio.h"

void debug(const char* fmt, ... );
void debug_heap( FILE* f,  void const* ptr );

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_MEM_DEBUG_H
