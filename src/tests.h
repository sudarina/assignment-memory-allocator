#include "mem_internals.h"

#define INIT_HEAP_SIZE 8192

/*Обычное успешное выделение памяти.*/
void test_1(struct block_header *block);

/*Освобождение одного блока из нескольких выделенных.*/
void test_2(struct block_header *block);

/*Освобождение двух блоков из нескольких выделенных.*/
void test_3(struct block_header *block);

/*Память закончилась, новый регион памяти расширяет старый.*/
void test_4(struct block_header *block);

/*Память закончилась, старый регион памяти не расширить из-за
  другого выделенного диапазона адресов, новый регион выделяется в другом месте.*/
void test_5(struct block_header *block);
