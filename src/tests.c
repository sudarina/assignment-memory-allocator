#define _DEFAULT_SOURCE

#include "tests.h"
#include <stdio.h>
#include "mem.h"
#include "mem_internals.h"
#include "mem_debug.h"
#include "util.h"

#define SIZE_QUERY_1 1024
#define SIZE_QUERY_2 2048
#define SIZE_QUERY_3 4096
#define SIZE_QUERY_4 8192

static struct block_header *get_block_header (void *data){
    return (struct block_header*) ((uint8_t *) data - offsetof(struct block_header, contents));
}

void test_1(struct block_header *block) {
    debug("Test 1 started\n");
    void *data = _malloc(SIZE_QUERY_3);
    if (data == NULL) {
        err("Test 1 failed: invalid data\n");
    }
    debug_heap(stdout, block);
    if (block->is_free) {
        err("Test 1 failed: block is still free\n");
    }
    if(block->capacity.bytes != SIZE_QUERY_3){
        err("Test 1 failed: invalid capacity\n");
    }
    debug("Test 1 passed\n______________\n");
    _free(data);

}

void test_2(struct block_header* block) {
    debug("Test 2 started\n");
    void *data_1 = _malloc(SIZE_QUERY_1);
    void *data_2 = _malloc(SIZE_QUERY_2);
    if (data_1 == NULL || data_2 == NULL) {
        err("Test 2 failed: invalid data\n");
    }

    _free(data_1);
    debug_heap(stdout, block);

    struct block_header *block_1 = get_block_header(data_1);
    struct block_header *block_2 = get_block_header(data_2);

    if(block_1->is_free || !block_2->is_free) {
        _free(data_2);
        debug("Test 2 passed\n______________\n");
    } else {
        err("Test 2 failed: incorrect result\n");
    }

}

void test_3(struct block_header* block) {
    debug("Test 3 started\n");
    void *data_1 = _malloc(SIZE_QUERY_1);
    void *data_2 = _malloc(SIZE_QUERY_1);
    void *data_3 = _malloc(SIZE_QUERY_1);
    if (data_1 == NULL || data_2 == NULL || data_3 == NULL) {
        err("Test 3 failed: invalid data\n");
    }
    _free(data_2);
    _free(data_1);
    debug_heap(stdout, block);
    struct block_header* block_1 = get_block_header(data_1);
    struct block_header* block_3 = get_block_header(data_3);

    if (!block_1->is_free){
        err("Test 3 failed: block 1 should be free^ but it's not\n");
    }
    if (block_3->is_free){
        err("Test 3 failed: block 3 shouldn't be free, but it is\n");
    }

    if (block_1->capacity.bytes != SIZE_QUERY_1 * 2 + offsetof(struct block_header, contents)){
        err("Test 3 failed: block's 1 capacity is incorrect\n");
    }
    if (block_3->capacity.bytes != SIZE_QUERY_1){
        err("Test 3 failed: block's 3 capacity is incorrect\n");
    }

    _free(data_3);
    debug("Test 3 passed\n______________\n");
}

void test_4(struct block_header* block) {
    debug("Test 4 started\n");
    void *data_1 = _malloc(SIZE_QUERY_1);
    void *data_2 = _malloc(SIZE_QUERY_3);
    void *data_3 = _malloc(SIZE_QUERY_3);
    if (data_1 == NULL || data_2 == NULL || data_3 == NULL){
        err("Test 4 failed: invalid data\n");
    }

    debug_heap(stdout, block);

    struct block_header* block_1 = get_block_header(data_1);
    struct block_header* block_2 = get_block_header(data_2);

    if ((uint8_t *) block_1->contents + block_1->capacity.bytes != (uint8_t *) block_2){
        err("Test 4 failed: 2nd block's address is incorrect\n");
    }

    _free(data_1);
    _free(data_2);
    _free(data_3);

    debug("Test 4 passed\n______________\n");
}

void test_5(struct block_header* block){
    debug("Test 5 started\n");
    void *data_1 = _malloc(SIZE_QUERY_4);
    if (data_1 == NULL){
        err("Test 5 failed: invalid data\n");
    }
    struct block_header* block_last = block;
    while (block_last->next != NULL) {
        block_last = block_last->next;
    }
    map_pages(block_last, 1024, 0);
    void *data_2 = _malloc(3 * SIZE_QUERY_4);
    debug_heap(stdout, block);

    struct block_header *block_2 = get_block_header(data_2);
    if (block_2 == block_last) {
        err("Test 5 failed: incorrect addressing\n");
    }
    debug("Test 5 passed\n______________\n");
    _free(data_1);
    _free(data_2);
}
