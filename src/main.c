//
// Created by Daria Supriadkina on 02.01.2022.
//
#include "tests.h"
#include "mem_debug.h"
#include "util.h"
#include "mem.h"


int main() {
    void *heap = heap_init(INIT_HEAP_SIZE);
    if (heap == NULL){
        err("Can't init heap\n");
    }
    test_1(heap);
    test_2(heap);
    test_3(heap);
    test_4(heap);
    test_5(heap);
    debug("Success!\n");
    return 0;
}
